module "sso_devops" {
  source  = "./devops"

  #for_each = toset(var.accounts["prod"])
  accounts            = var.accounts["prod"]
  sso_instance_arn    = var.sso_instance_arn
  set_name            = var.devops_set_name
  set_description     = var.devops_set_description
  sso_relay_state     = var.sso_relay_state
  session_duration    = var.session_duration
  tags                = var.tags
  inline_policy       = var.devops_inline_policy
  managed_policy_arn  = var.devops_managed_policy_arn

}
