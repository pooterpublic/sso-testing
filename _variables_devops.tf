variable "devops_set_name" {
  description = "Name of permission set"
  type = string
  value = ""
}

variable "devops_set_description" {
  description = "Description of permission set"
  type = string
  value = ""
}

variable "devops_tags" {
  description = "Tags assigned to the permission set"
  type = string
  value = ""
}

variable "devops_inline_policy" {
  description = "ARN of the inline policy you are attaching to the permission set"
  type = string
  value = ""
}

variable "devops_managed_policy_arn" {
  description = "ARN of the managed policy you are attaching to the permission set"
  type = string
  value = ""
}
