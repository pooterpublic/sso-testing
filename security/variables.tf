variable "accounts" {}

variable "sso_instance_arn" {}

variable "security_set_name" {}

variable "security_set_description" {}

variable "sso_relay_state" {}

variable "session_duration" {}

variable "tags" {}

variable "security_inline_policy" {}

variable "security_managed_policy_arn" {}