module "sso_readonly_with_support" {
  source  = "./readonly_with_support"

  accounts            = var.accounts["all"]
  sso_instance_arn    = var.sso_instance_arn
  set_name            = var.readonly_with_support_set_name
  set_description     = var.readonly_with_support_set_description
  sso_relay_state     = var.sso_relay_state
  session_duration    = var.session_duration
  tags                = var.tags
  inline_policy       = var.readonly_with_support_inline_policy
  managed_policy_arn  = var.readonly_with_support_managed_policy_arn
}
