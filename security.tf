module "sso_security" {
  source  = "./security"

  #for_each = toset(var.accounts["prod"])
  accounts            = var.accounts["prod"]
  sso_instance_arn    = var.sso_instance_arn
  set_name            = var.security_set_name
  set_description     = var.security_set_description
  sso_relay_state     = var.sso_relay_state
  session_duration    = var.session_duration
  tags                = var.tags
  inline_policy       = var.security_inline_policy
  managed_policy_arn  = var.security_managed_policy_arn

}
