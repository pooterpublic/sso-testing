provider "aws" {
  region = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

data "aws_caller_identity" "current" {}

/*
module "sso" {
  source  = "binbashar/sso/aws"
  version = "0.0.1"

  for_each = toset(var.accounts["prod"])

  # Common configuration for all permission sets
  # ...
}
*/