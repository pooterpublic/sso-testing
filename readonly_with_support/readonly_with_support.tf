locals {
  sso_permission_sets = {
    readonly_with_support = {
      inline_policy       = file("${path.module}/iprestrictions.json")
      managed_policy_arns = [aws_iam_policy.ReadOnly.arn, aws_iam_policy.SupportUser.arn]
      session_duration    = var.session_duration
      relay_state         = var.sso_relay_state
      tags                = var.tags
    }
  }

  sso_group_assignments = {
    readonly_with_support = var.groups
  }
}

resource "aws_ssoadmin_permission_set" "this" {
  for_each = local.sso_permission_sets

  name             = each.key
  description      = each.value.description
  instance_arn     = var.sso_instance_arn
  relay_state      = each.value.relay_state
  session_duration = each.value.session_duration
  tags             = each.value.tags
}

resource "aws_ssoadmin_permission_set_inline_policy" "this" {
  for_each = local.sso_permission_sets

  inline_policy      = each.value.inline_policy
  instance_arn       = var.sso_instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.this[each.key].arn
}

resource "aws_ssoadmin_managed_policy_attachment" "this" {
  for_each = {
    for key, value in local.sso_permission_sets : key => value
    for managed_policy_arn in value.managed_policy_arns
  }

  instance_arn       = var.sso_instance_arn
  managed_policy_arn = managed_policy_arn
  permission_set_arn = aws_ssoadmin_permission_set.this[each.key].arn
}

resource "aws_ssoadmin_account_assignment" "this" {
  for_each = local.sso_group_assignments

  instance_arn       = var.sso_instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.this[each.key].arn
  principal_id       = data.aws_identitystore_group.this[each.value].group_id
  principal_type     = "GROUP"
  target_id          = var.accounts
  target_type        = "AWS_ACCOUNT"
}

data "aws_identitystore_group" "this" {
  for_each = local.sso_group_assignments

  identity_store_id = each.value
}

data "aws_iam_policy" "ReadOnly" {
  arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

data "aws_iam_policy" "SupportUser" {
  arn = "arn:aws:iam::aws:policy/AWSSupportAccess"
}
