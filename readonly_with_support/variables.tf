variable "accounts" {}

variable "sso_instance_arn" {}

variable "readonly_set_name" {}

variable "readonly_set_description" {}

variable "sso_relay_state" {}

variable "session_duration" {}

variable "tags" {}

variable "readonly_inline_policy" {}

variable "readonly_managed_policy_arn" {}

