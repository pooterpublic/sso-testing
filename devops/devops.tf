sso_permission_sets = {
    "DevOpsSuperUser" = {
      description = "DevOps Super User"
      session_duration = "PT12H"
      relay_state = "https://console.aws.amazon.com/console/home"
      inline_policy = jsonencode(
        merge(
          jsondecode(file("abac.json"))
          #jsondecode(file("iprestrictions.json"))
        )
      )
    },
  }

  sso_group_assignments = {
    "DevOps" = {
      sso_instance_arn = module.sso.sso_instance_arn
      sso_permission_sets = [
        module.sso.sso_permission_sets["DevOpsSuperUser"]
      ]
    }
  }

  tags = {
    Terraform   = "true"
    Environment = "prod"
  }

  resource "aws_ssoadmin_account_assignment" "this" {
  instance_arn       = var.sso_instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.this.arn

  principal_id   = data.aws_identitystore_group.this[each.value.sso_group].group_id
  principal_type = "GROUP"

  target_id   = var.account_id
  target_type = "AWS_ACCOUNT"
  }

  #-----------------------------------------------------------------------------------------------------------------------
# CREATE THE PERMISSION SETS
#-----------------------------------------------------------------------------------------------------------------------
resource "aws_ssoadmin_permission_set" "this" {
  name             = var.devops_set_name
  description      = var.devops_set_description
  instance_arn     = var.sso_instance_arn
  relay_state      = var.sso_relay_state
  session_duration = var.session_duration
  tags             = var.tags
}

#-----------------------------------------------------------------------------------------------------------------------
# ATTACH INLINE POLICIES
#-----------------------------------------------------------------------------------------------------------------------
resource "aws_ssoadmin_permission_set_inline_policy" "this" {
  inline_policy      = jsonencode(
                          merge(
                            jsondecode(file("abac.json"))
                            #jsondecode(file("iprestrictions.json"))
                          )
                        )
  instance_arn       = var.sso_instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.this.arn
}

#-----------------------------------------------------------------------------------------------------------------------
# ATTACH MANAGED POLICIES
#-----------------------------------------------------------------------------------------------------------------------
resource "aws_ssoadmin_managed_policy_attachment" "this" {
  instance_arn       = var.sso_instance_arn
  managed_policy_arn = var.devops_managed_policy_arn
  permission_set_arn = aws_ssoadmin_permission_set.this.arn
}

#-----------------------------------------------------------------------------------------------------------------------
# ATTACH CUSTOMER MANAGED POLICIES
#-----------------------------------------------------------------------------------------------------------------------
resource "aws_ssoadmin_customer_managed_policy_attachment" "this" {
  instance_arn       = var.sso_instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.this.arn
  customer_managed_policy_reference {
    name = each.value.policy_name
    path = each.value.policy_path
  }
}