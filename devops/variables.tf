variable "accounts" {}

variable "sso_instance_arn" {}

variable "devops_set_name" {}

variable "devops_set_description" {}

variable "sso_relay_state" {}

variable "session_duration" {}

variable "tags" {}

variable "devops_inline_policy" {}

variable "devops_managed_policy_arn" {}