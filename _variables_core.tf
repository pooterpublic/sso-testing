variable "accounts" {
  description = "Map of accounts categorized by environment"
  type = map(list(string))
}

variable "aws_access_key" {
  description = "AWS Access Key ID"
  type        = string
  sensitive   = true
}

variable "aws_secret_key" {
  description = "AWS Secret Access Key"
  type        = string
  sensitive   = true
}

variable "sso_instance_arn" {
  description = "SSO instance ARN"
  type = string
  value = "arn:aws:sso:::instance/ssoins-6684b4fe40983b06"
}

variable "sso_relay_state" {
  description = "Relay state of SSO instance"
  type = string
  value = ""
}

variable "session_duration" {
  description = "Duration of SSO session"
  type = string
  value = ""
}

variable "tags" {
  description = "Tags assigned to the permission set"
  type = string
  value = ""
}

